Title: 2014-05-13
Date: 2014-05-13
Tags: darkplaces, engine, q3bsp

New darkplaces update:

- Revised collision code for q3bsp again, `collision_enternudge`/`collision_leavenudge` cvars have been set back to `0` and removed due to problems on terrain maps such as the Nexuiz map `ons-reborn`, the other collision improvements should still help significantly.

