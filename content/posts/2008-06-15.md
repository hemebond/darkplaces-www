Title: 2008-06-15
Date: 2008-06-15
Tags: dpmaster

Posted Elric's new dpmaster version 1.7, featuring a perl-based test suite, several bugfixes and minor one-time memory leak fixes, increases in default server limits, corrections to techinfo.txt, and other improvements.


(Note: this server-list database program is only interesting to independent game development teams and tournament administrators)