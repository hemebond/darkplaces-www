Title: 2014-04-30
Date: 2014-04-30
Tags: darkplaces, engine

New darkplaces update:

- FXAA added: `r_fxaa 1` in console to activate it (thanks graphitemaster)
- `r_glsl_postprocess` will no longer force a blur effect unless the corresponding uservecs are in use (thanks divVerent).
- Updated ip address for master server `dpmaster.deathmask.net` and added ip6 address for it as well (someday we'll use threaded dns lookup instead).
- Increased `effectinfo.txt` limits as requested by JKoestler who managed to have *that* many effects defined.
- Allow .rtlights files to use style -1 for compatibility with fteqw-produced rtlights files.
- New `vid_desktopfullscreen` cvar will use your desktop resolution instead of changing video mode, this is better behaved on all platforms but especially Linux.
- `gl_vbo_dynamicvertex` and `gl_vbo_dynamicindex` have been optimized (but seem to still be slower on desktop GL than using conventional vertex arrays).
- Fixed some issues with the unmaintained D3D9 renderer pertaining to `vid_sRGB` and `vid_stereobuffer`.
- Fixed a bug with `EF_FLAME` and `EF_STARDUST` effects still emitting particles when paused.
- Changed behavior when stuck in brush models such that you can will not be stuck in an entity such as a door or platform, only stuck if in solid world geometry (an intentional quake behavior).
