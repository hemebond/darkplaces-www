Title: 2004-10-16
Date: 2004-10-16
Tags: darkplaces, engine

New darkplaces release with various bugfixes, what's new:

- Now detects a few quake3 shader types to make quake3 maps look mostly correct (additive shaders are detected, as well as autosprite/autosprite2, and twosided shaders, also loads first pass texture if it can't find a texture of the same name as the shader, and transparent textures in quake3 maps now require a shader to indicate they are transparent).

- Fixed a bug that was preventing dynamic lights from lighting up quake3 maps.

- Now prints 8 QuakeC opcodes instead of 5 when a crash or warning occurs.

- MoALTz fixed the `r_stereo_separation` code so the anaglyph stereo glasses support works properly now.

- Rewrote chat message handling code to be cleaner.

- Added DP_LITSPRITES extension string (the feature existed already but was not documented).

- Fixed connect status messages in join game menu (it was only showing them in server list menu).

- Simplified `cl_net*` cvars to just `cl_netlocalping` and `cl_netpacketloss` and removed `sv_netpacketloss` cvars.

- Made tenebrae light cubemaps work on model entities again (they were being disabled because the requested skin did not exist in the model, so the cubemap was being set to 0).

- Greatly optimized findradius builtin (it now uses the physics culling data to avoid searching all entities).

- Removed `cl_nodelta` cvar as it never worked properly, and is unnecessary with the current network protocol.

- Fixed a bug that made the game world seem to freeze after a level change in multiplayer (networking was falling apart).

- Fixed framegroup animation on normal entities (static entities like Quake's torches worked already).

- Changed movement interpolation a bit to better handle less frequent updates on far away entities.

- Fixed a "blue dlight" bug when a map uses tenebrae dlights but the progs.dat did not support them (it was a silly typo in the engine).

- Fixed a crash when dedicated servers tried to talk to the players.

- Fixed a "blue glow_trail" bug, it was not properly interpreting the palette color.



