Title: 2004-10-06
Date: 2004-10-06
Tags: darkplaces, engine

New darkplaces release with various bugfixes, what's new:

- fixed lingering entities in quake protocol, demos for example

- added `cl_beams_*` cvars to effects options menu

- now can connect to another server while remaining connected to current server up until the connection is accepted

- now shows loading screen at appropriate times which also stops sound

- fixed nehahra movie support

- sprites now support EF_NODEPTHTEST

- watershader now disabled on lightmapped water

- now compiles on x86_64 successfully, server still crashes though (will be fixed at some point)

- added commandline options to readme

- fixed an entity colormapping bug which caused players to be white in dpmod

- skybox works in hlbsp again

- fixed weird player model angles when looking down

- negative frags display correctly again

- Mathieu fixed a logging problem which was not recording cvar creation notices to the log

- Mathieu fixed sound channel volume clipping to work the same on 16bit and 8bit sounds

- removed detection of GL_NV_vertex_array_range as it's not used

- added `-novideosync` disable for WGL swap control

- fixed qc builtin cvar_string to not crash engine if the cvar does not exist



