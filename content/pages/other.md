Title: Other
Summary: Miscellaneous
Banner: e2m3.jpg
Template: page



Beta releases, old versions, and miscellaneous mods I made are in the [files](https://icculus.org/twilight/darkplaces/files/) directory, this page only lists current versions of darkplaces and some utilities.

You can also find a lot of old fun Quake stuff on idgames2 mirrors, there's a full one over at [mancubus.net](http://ftp.mancubus.net/pub/idgames2)


## Quake Art Enhancement Projects

[Rygel's 2.7GB ultra quality pack](https://icculus.org/twilight/darkplaces/files/rygel-dp-texturepack-ultra.pk3), ready to drop into a quake gamedir such as id1, or [grab the 900MB high quality version](https://icculus.org/twilight/darkplaces/files/rygel-dp-texturepack-high.pk3) which loads much faster and is usable on video cards with less than 1GB video ram.

[Quake Retexture/Remodel Project](http://qrp.quakeone.com/) (maintained by Up2nOgOoD[ROCK] as primarily a QuakeWorld content replacement project, difficult to install on DarkPlaces, formerly known as the QE1M1/QE1 project made famous by Tenebrae).

[Romi's rtlights file for Quake](https://icculus.org/twilight/darkplaces/files/romirtlights_id1.pk3) (rtlights pack for Quake - drop this file in your id1 directory to get improved performance in all id1 maps when using Realtime World Lighting in the options menu)

[Romi's rtlights file for Quake Mission Pack 1](https://icculus.org/twilight/darkplaces/files/romirtlights_soa.pk3) (rtlights pack for Scourge of Armagon - by Ritual Entertainment, formerly known as Hipnotic Entertainment)

## Utilities

### hmap2

[hmap2 build 20121222](https://icculus.org/twilight/darkplaces/files/hmap2build20121222.zip) useful for compiling maps with colored lighting, rotating doors, and lots of other features, check out the readme, extremely high limits (if map really is too complex for quake limits it will write BSP2 format for use in darkplaces)

[dpmaster 2.2](https://icculus.org/twilight/darkplaces/files/dpmaster-2.2.zip) DarkPlaces, Quake3, Return To Castle Wolfenstein, Wolfenstein: Enemy Territory, and QFusion master server program written by Mathieu Olivier, supported by the net_inetslist command in darkplaces as well as the menu (lists all servers that have sv_public 1 set, if they are using the master servers you are using), includes windows executable as well as source code which compiles on Linux and BSD.

[lmp2pcx build 20070412](https://icculus.org/twilight/darkplaces/files/lmp2pcx20070412.zip) converts every .lmp, .mip and .wad file in the current directory to .pcx, .tga, .lmp, and .mip files

[lhfire build 20070412](https://icculus.org/twilight/darkplaces/files/lhfire20070412.zip) particle effect renderer used for the explosions, plasma shots, and other sprites in the mod, can make .spr, .spr32, and .tga output, read the included scripting info for use

[setgamma 1.1 (Windows only)](https://icculus.org/twilight/darkplaces/files/setgamma11.zip) commandline gamma tweaking utility for whatever uses one can imagine, has special support for 3DFX voodoo1/2 addon cards

[md3 model tool and makesp2 build 20070412](https://icculus.org/twilight/darkplaces/files/modeltools20070412.zip) modeltool is useful for renaming shaders within an md3 model, and changing the flags value to add trail flags like 1 for rocket, 2 for grenade, 4 for gib, 8 for rotating pickup item, etc - for more flags search for EF_ROCKET in protocol.h and read the other values, makesp2 makes a quake2 .sp2 sprite file (which does not include images) given commandline options (does not access the images, the dimensions can be completely different)

[.dpm and .md3 model compiler utility for mod developers](https://icculus.org/twilight/darkplaces/files/dpmodel20080715.zip) reads a .txt script listing .smd files to compile into a .dpm model file and .md3 model file, see included documentation for example script and documentation, smd is produced by Half-Life(tm) 1/2 model exporters available for some 3D modeling programs, it is a skeletal model format

[.dpm model viewer utility for mod developers](https://icculus.org/twilight/darkplaces/files/dpmviewer20070412.zip) displays a .dpm model with texturing, allows you to view animations and rotate the model, but not much else

[.tga alpha fixer](https://icculus.org/twilight/darkplaces/files/lhtgatools20070412.zip) this program can fix broken tga images written by certain paint programs such as Paint Shop Pro(r), this repairs transparent images that do not appear transparent in darkplaces, and clears unused palette information in truecolor images

## Patches for Broken Mods

[Bugfixed TargetQuake](https://icculus.org/twilight/darkplaces/files/targetquakedarkplaces.zip) this is a full version (not a patch) of the famous platform game TargetQuake fixed up to work properly in DarkPlaces engine, many bugs fixed, mostly to do with setmodel being called without setsize

[Bugfix patch for X-Men: Ravages of Apocalypse](https://icculus.org/twilight/darkplaces/files/X-MenROA_SourceCode_bugfixes20060705.zip) this is a bugfix patch for the formerly commercial Quake total conversion [X-Men: Ravages of Apocalypse](http://www.zerogravity.com.au/xmen/), it fixes many stupid bugs in the qc code, mostly related to setmodel being called without setsize, however it does not magically turn it into a good game :)

## Other Things LadyHavoc Made or Worked On

[Alien Infestation speedmod from QuakeExpo 2006](https://icculus.org/twilight/darkplaces/files/alieninfestation.zip) a speedmod with a custom map using the models provided in the speedmod theme, alien eggs (which spawn even in deathmatch) on the ceiling drop facehugger-like critters, and also watch out for the security droids

[.dlit files for id1 maps](https://icculus.org/twilight/darkplaces/files/deluxemaps_id1.pk3) this pk3 allows you to use bumpmapped/glossmapped wall textures without turning on realtime world lighting, requires OpenGL Shading Language capable card, such as GeForceFX 5200 or above or Radeon 9500 or above, note: this pack does not have colored lighting, it is simply hmap2 -relight done on each map

[quake 1.06 qc source](https://icculus.org/twilight/darkplaces/files/id1qc.zip) this is just the quake 1.06 qc source, a decent starting point for making new mods, simply unzip to quake/mymod/qc/ and edit to taste, use frikqcc or fteqcc to compile the code, you can find those with a web search

[quake 1.06 qc source without singleplayer](https://icculus.org/twilight/darkplaces/files/mponlyqc.zip) same as the above source in every way except lacking monster code and everything associated with it, preserves all features in deathmatch, this may be a better starting point for multiplayer-only mods

[Quake Deathmatch Revelation experimental deathmatch mod](https://icculus.org/twilight/darkplaces/files/revelation20070311.zip) this is an experimental mod testing out some modifications to standard Quake deathmatch: ammo, health, and armor regenerate, you hear a noise when you do damage to another player, explosions have a larger splash radius but do less damage for splash, no items spawn, and the weapons are rebalanced to be more equal to eachother

[Rocket Symphony experimental deathmatch mod from QuakeExpo 2004](https://icculus.org/twilight/darkplaces/files/2004/rocketsymphony20040703.zip) this mod is played from an overhead perspective and replaces all the quake weapons with various forms of rocket launcher, firing in many different shot patterns, including spiral attacks, it is inspired by the boss battles in the vertical shooter game Ikaruga(r), and is meant to simulate dueling bosses, but it needs a very wide open map with a large flat floor, dpdm2 sort of works for this purpose, but I'd appreciate if someone made a better map for this mod

[Nexuiz](http://www.alientrap.org/nexuiz) completely open source online deathmatch game using the DarkPlaces engine, does not require Quake

## Maps

Some included in darkplaces mod, but are playable without it.

[dpdm1](https://icculus.org/twilight/darkplaces/files/dpdm1.zip) a very small and brutal deathmatch map.

[dpdm2](https://icculus.org/twilight/darkplaces/files/dpdm2.zip) a very large in-progress deathmatch map.

[LadyHavoc's old maps](https://icculus.org/twilight/darkplaces/files/ladyhavocmaps.pk3) a collection of old maps (not DarkPlaces related) by LadyHavoc:

- ctfgold8
- lhca1
- lhdm4
- metlhell
- rampcity

[entitytest.bsp](https://icculus.org/twilight/darkplaces/files/entitytest.zip) test map testing map created years ago to test a report of darkplaces crashing when too many nailgun models were on screen, never found the crash, but this is kind of an interesting test map as it has a moving sky brush which gives most engines a heartattack, and demonstrates large colored flashing lights, warning: there's a lone fiend in this map in singleplayer.

[cavetest2.bsp](https://icculus.org/twilight/darkplaces/files/cavetest2.zip) testing map created years ago to push the limits of the q1bsp nearly to the point of breaking, crashes most engines at load, runs poorly, has over 26000 surfaces and over 10000 leafs in a single room, and it is fully vised (which actually didn't take that long) and lit, but barely playable, warning: in singleplayer this is nearly packed wall to wall with monsters, plays better in deathmatch.

## Engine and Mod Developer Examples

[sv_user.qc for mod developers](https://icculus.org/twilight/darkplaces/files/sv_user.qc) direct port of player physics code from engine to QC (this code is called by darkplaces engine if found, allowing great flexibility), highly optimized and more readable, no behavior changes, modify this for your own mods and enjoy.

[.lit colored lighting data support tutorial for engine coders](https://icculus.org/twilight/darkplaces/files/2004/litsupport20040521.zip) all changes commented with LadyHavoc, changes engine to use RGB colored lighting internally, supports static (.lit) colored lighting and dynamic, models are properly lit in color as well.

[particle font texture for mod developers](https://icculus.org/twilight/darkplaces/files/particlefont.tga.zip) example image to start with when customizing particle textures, be sure to keep directories when extracting to your mod directory.
